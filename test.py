import requests
url = 'http://jwehling.pythonanywhere.com/position'
headers = {'Content-Type': 'application/json'}

new_data = '''
{
  "0": {
    "number": 0,
    "position": {
        "x": 3.3,
        "y": 5,
        "Z": 0
    }
  },
  "1": {
    "number": 1,
    "position": {
        "X":5,
        "Y": 3.2,
        "z": 0
    }
  }
}
'''

partial_update_data = '''
{
  "0": {
    "number": 0,
    "position": {
        "X": 3.3
    }
  },
  "1":{
    "position": {
        "y":5
    }
  }
}
'''

get_resp = requests.get(url)
create_resp = requests.put(url, data=new_data, headers=headers)
incomplete_create_resp = requests.put(url, data=partial_update_data, headers=headers)
update_resp = requests.patch(url, data=partial_update_data, headers=headers)
delete_resp = requests.delete(url)

print get_resp.status_code
print get_resp.content
print create_resp.status_code
print create_resp.content
print incomplete_create_resp.status_code
print incomplete_create_resp.content
print update_resp.status_code
print update_resp.content
print delete_resp.status_code
print delete_resp.content
