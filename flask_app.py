from flask import Flask, request, jsonify
from flask_restful import Api, Resource


app = Flask(__name__)
api = Api(app)


class Players(object):

    def __init__(self, player_0=None, player_1=None):
        self.player_0 = player_0 or PlayerPosition(0)
        self.player_1 = player_1 or PlayerPosition(1)

    def update_by_dict(self, body):
        play_0_dict = body.get('0', None)
        play_1_dict = body.get('1', None)
        
        if self.player_0 is None:
            self.player_0 = PlayerPosition(0)
        if self.player_1 is None:
            self.player_1 = PlayerPosition(1)
        
        if play_0_dict is not None:
            self.player_0.update_by_dict(play_0_dict)
        if play_1_dict is not None:
            self.player_1.update_by_dict(play_1_dict)

    def check_strict(self, body):
        strict_passes = True
        play_0_dict = body.get('0', None)
        play_1_dict = body.get('1', None)
        
        if play_0_dict is None:
            body['0'] = 'MISSING'
            strict_passes = False
        else:
            if self.player_0 is None:
                self.player_0 = PlayerPosition(0)
            status, fails_dict = self.player_0.check_strict(play_0_dict)
            strict_passes &= status
            if not status:
                body['0'] = fails_dict
        
        if play_1_dict is None:
            body['1'] = 'MISSING'
            strict_passes = False
        else:
            if self.player_1 is None:
                self.player_1 = PlayerPosition(1)
            status, fails_dict = self.player_1.check_strict(play_1_dict)
            strict_passes &= status
            if not status:
                body['1'] = fails_dict
        
        return (strict_passes, body)
        
    def to_dict(self):
        return {0: self.player_0.to_dict(), 1: self.player_1.to_dict()}


class PlayerPosition(object):

    def __init__(self, number, pos=None):
        self.number = number
        self.pos = pos or Coords()

    def update_by_dict(self, body):
        number = body.get('number', None)
        position_dict = body.get('position', None)
            
        if self.pos is None:
            self.pos = Coords()
            
        if number is not None:
            self.number = int(number)
        if position_dict is not None:
            self.pos.update_by_dict(position_dict)

    def check_strict(self, body):
        strict_passes = True
        number = body.get('number', None)
        position_dict = body.get('position', None)
        
        if number is None:
            body['number'] = 'MISSING'
            strict_passes = False
        
        if position_dict is None:
            body['position'] = 'MISSING'
            strict_passes = False
        else:
            if self.pos is None:
                self.pos = Coords()
            status, fails_dict = self.pos.check_strict(position_dict)
            strict_passes &= status
            if not status:
                body['position'] = fails_dict
        
        return (strict_passes, body)
        
    def to_dict(self):
        return {'number': self.number, 'position': self.pos.to_dict()}


class Coords(object):
    needs_params = ['x', 'y', 'z']
    sub_models = []

    def __init__(self, x=0, y=0, z=0):
        self.x = x
        self.y = y
        self.z = z
        
    def update_by_dict(self, body):
        x = body.get('x', None) or body.get('X', None)
        y = body.get('y', None) or body.get('Y', None)
        z = body.get('z', None) or body.get('Z', None)
            
        if x is not None:
            self.x = x
        if y is not None:
            self.y = y
        if z is not None:
            self.z = z

    def check_strict(self, body):
        strict_passes = True
        x = body.get('x', None)
        if x is None:
            x = body.get('X', None)
        y = body.get('y', None)
        if y is None:
            y = body.get('Y', None)
        z = body.get('z', None)
        if z is None:
            z = body.get('Z', None)
        
        if x is None:
            body['x'] = 'MISSING'
            strict_passes = False
        if y is None:
            body['y'] = 'MISSING'
            strict_passes = False
        if z is None:
            body['z'] = 'MISSING'
            strict_passes = False
        
        return (strict_passes, body)

    def to_dict(self):
        return {'x': self.x, 'y': self.y, 'z': self.z}


class Position(Resource):
    state = Players()

    def get(self):
        return self.state.to_dict()

    def put(self):
        data = request.json
        passes_strict, fails_dict = self.state.check_strict(data)
        if not passes_strict:
            return {'message': {'Invalid Request': fails_dict}}, 400
        else:
            self.state.update_by_dict(data)
            return self.state.to_dict()

    def delete(self):
        self.state = Players()
        return self.state.to_dict()

    def patch(self):
        data = request.json
        self.state.update_by_dict(data)
        return self.state.to_dict()


api.add_resource(Position, '/position')


if __name__ == '__main__':
    app.run(debug=True)
